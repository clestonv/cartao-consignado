Feature: Proposta

    Como usuario, desejo enviar a proposta
    Esqueceu de preencher os campos

Scenario: Esqueceu de preencher todos os campos
    Given que eu acesso o site   
    When clico para enviar a proposta
    Then tenho que receber "24" avisos de campo vazio

Scenario: Preencheu só o nome
    Given que eu acesso o site
    When preencho só o "Cleberson"
    And e clico no proximo campo
    Then tenho que receber  a mensagem "Preecha nome e sobrenome."

Scenario: Preencheu só o nome completo
    Given que eu acesso o site
    When preencho só o "Cleberson Osorio"
    When clico para enviar a proposta
    Then tenho que receber "23" avisos de campo vazio

Scenario: Preencheu só o nome completo e CPF
    Given que eu acesso o site
    When preencho só o "Cleberson Osorio" e "383.381.528-03"
    When clico para enviar a proposta
    Then tenho que receber "22" avisos de campo vazio

Scenario: Preencheu só o nome completo e CPF, RG
    Given que eu acesso o site
    When preencho só o "Cleberson Osorio" e "383.381.528-03", "445524418"
    When clico para enviar a proposta
    Then tenho que receber "21" avisos de campo vazio
