Given(/^que eu acesso o site$/, () => {
	cy.visit('/')
});

When(/^preencho só o "([^"]*)"$/, (nome) => {
	cy.get('#nome').type(nome)
});


When(/^preencho só o "([^"]*)" e "([^"]*)"$/, (nome,cpf) => {
    cy.get('#nome').type(nome)
    cy.get('#cpf').type(cpf)
});


When(/^preencho só o "([^"]*)" e "([^"]*)", "([^"]*)"$/, (nome,cpf,rg) => {
    cy.get('#nome').type(nome)
    cy.get('#cpf').type(cpf)
    cy.get('#rg').type(rg)
});


When(/^clico para enviar a proposta$/, () => {
	cy.get('#botao-enviar').click()
});

Then(/^tenho que receber "([^"]*)" avisos de campo vazio$/, (respostas) => {	
    cy.get('.wpcf7-not-valid-tip').as('classes')
    cy.get('@classes').should('to.have.length', respostas)
});


When(/^e clico no proximo campo$/, () => {
	cy.get('#cpf').click()
});


Then(/^tenho que receber  a mensagem "([^"]*)"$/, (msg) => {
    cy.get('.modal-body').as('mensagem')
    
    cy.get('@mensagem')
        .invoke('text')
        .should('be.equal', msg)
});

